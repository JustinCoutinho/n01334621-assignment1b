﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1a
{
    public class Member
    {
        
        private string memberFname;
        private string memberLname;
        private string memberPhone;
        private string memberEmail;
        private string memberAddress;
        private string memberGender;
        private string memberAge;
        private string memberType;
        

        public Member()
        {
        }
        public string MemberFname
        {
            get { return memberFname; }
            set { memberFname = value; }
        }
        public string MemberLname
        {
            get { return memberLname; }
            set { memberLname = value; }
        }
        public string MemberPhone
        {
            get { return memberPhone; }
            set { memberPhone = value; }
        }
        public string MemberEmail
        {
            get { return memberEmail; }
            set { memberEmail = value; }
        }
        public string MemberAddress
        {
            get { return memberAddress; }
            set { memberAddress = value; }
        }
        public string MemberGender
        {
            get { return memberGender; }
            set { memberGender = value; }
        }
        public string MemberAge
        {
            get { return memberAge; }
            set { memberAge = value; }
        }
        public string MemberType
        {
            get { return memberType; }
            set { memberType = value; }
        }
    }
}