﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1a
{
    public class Registration
    {
        public Member member;
        public Feedback feedback;
        public Registration(Member m, Feedback f)
        {
            member = m;
            feedback = f;
        }
   
        public string PrintReceipt()
        {
            string receipt = "Memebership Receipt:<br/>";
            receipt += "First Name: " + member.MemberFname + "<br/>";
            receipt += "Last Name: " + member.MemberLname + "<br/>";
            receipt += "Phone: " + member.MemberPhone + "<br/>";
            receipt += "Address: " + member.MemberAddress + "<br/>";
            receipt += "Gender: " + member.MemberGender + "<br/>";
            receipt += "Age: " + member.MemberAge + "<br/>";
            receipt += "Membership Type: " + member.MemberType + "<br/>";
            receipt += "Training Preference: " + String.Join(", ", feedback.training.ToArray()) + "<br/>";
            receipt += "Total: " + PackageTotal().ToString() + "<br/>";

            return receipt;
                                   
        }
        public double PackageTotal()
        {
            double total = 0;
            if (member.MemberType == "Basic")
            {
                total = 49.99;
            }
            else if (member.MemberType == "Premium")
            {
                total = 59.99;
            }
            else if (member.MemberType == "PremiumPlus")
            {
                total = 69.99;
            }
            //Calculates tax of total
            total = total * .13 + total;

            return total;
        }
    }
}