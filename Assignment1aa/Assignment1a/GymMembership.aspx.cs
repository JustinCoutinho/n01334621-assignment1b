﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment1a
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void SubmitBtn_Click(object sender, EventArgs e) 
        {
            
            List<string> training = new List<string>{};
            Feedback newfeedback = new Feedback(training);
          
            //This code references Christine's pizza example
            List<string> trainingpref = new List<string>();
            foreach (Control control in Training_Type.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox trainingtype = (CheckBox)control;
                    if (trainingtype.Checked)
                    {
                        trainingpref.Add(trainingtype.Text);
                    }
                }
            }
            newfeedback.training = newfeedback.training.Concat(trainingpref).ToList();

            int age = int.Parse(CustomerAge.Text);
            string size = memberType.SelectedItem.Value.ToString();
            string fname = CustomerFname.Text.ToString();
            string lname = CustomerLname.Text.ToString();
            string cphone = CustomerPhone.Text.ToString();
            string email = CustomerEmail.Text.ToString();
            string address = CustomerMailingAddress.Text.ToString();
            string gender = CustomerGender.Text.ToString();
            string membership = memberType.Text.ToString();
            
            Member newmember = new Member();
            newmember.MemberFname = fname;
            newmember.MemberLname = lname;
            newmember.MemberPhone = cphone;
            newmember.MemberEmail = email;
            newmember.MemberAge = age.ToString();
            newmember.MemberAddress = address;
            newmember.MemberGender = gender;
            newmember.MemberType = membership;
            
            Registration newreg = new Registration(newmember, newfeedback);
            NewRegistration.InnerHtml = newreg.PrintReceipt();


            

        }





      

          

        
        
    }
}