﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GymMembership.aspx.cs" Inherits="Assignment1a.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Very Big Muscles Gym Club</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        <asp:ValidationSummary ID="ValidSum" runat="server"/>
            <h1>Very Big Muscles Gym Club Registration</h1>
                <asp:TextBox runat="server" ID="CustomerFname" placeholder="First Name"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your first name" ControlToValidate="CustomerFname" ID="validatorFname"></asp:RequiredFieldValidator>
            <br />
                <asp:TextBox runat="server" ID="CustomerLname" placeholder="Last Name"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your last name" ControlToValidate="CustomerLname" ID="validatorLname"></asp:RequiredFieldValidator>
            <br />
                <asp:TextBox runat="server" ID="CustomerPhone" placeholder="Phone Number"></asp:TextBox>
                <asp:RegularExpressionValidator runat="server" ErrorMessage="Please enter a 10 digit number" ControlToValidate="CustomerPhone" ValidationExpression="^[0-9]{10}$"/>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your phone number" ControlToValidate="CustomerPhone" ID="validatorPhone"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ControlToValidate="CustomerPhone" Type="String" Operator="NotEqual" ValueToCompare="1234567890" ErrorMessage="This is not your phone number"></asp:CompareValidator>
            <br />
                <asp:TextBox runat="server" ID="CustomerEmail" placeholder="Email"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your Email" ControlToValidate="CustomerEmail" ID="RequiredEmail"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="EmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="CustomerEmail" ErrorMessage="That's not an email!"></asp:RegularExpressionValidator>
            <br />
                <asp:TextBox runat="server" ID="CustomerMailingAddress" placeholder="Address"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="CustomerMailingAddress" ErrorMessage="Please enter your address"></asp:RequiredFieldValidator>
            <br />
            <h2>Gender</h2>
            <asp:RadioButtonList ID="CustomerGender" runat="server">
                <asp:ListItem>Male</asp:ListItem>
                <asp:ListItem>Female</asp:ListItem>
                <asp:ListItem>Other</asp:ListItem>
            </asp:RadioButtonList>
            <br />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="customerGender" ErrorMessage="Please select a gender option"></asp:RequiredFieldValidator>
            <br />
                <asp:TextBox runat="server" ID="CustomerAge" placeholder="Age"></asp:TextBox>
                <asp:RangeValidator runat="server" ControlToValidate="CustomerAge" Type="Integer" MinimumValue="16" MaximumValue="100" ErrorMessage="Invalid age - you must be at least 16 to sign up without a parent"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your Age" ControlToValidate="CustomerAge" ID="AgeFieldValid"></asp:RequiredFieldValidator>
            
            <br />
            <h3>Membership Type</h3>
            <asp:DropDownList runat="server" ID="memberType">
                <asp:ListItem Value="Basic" Text="Basic Muscles $49.99"></asp:ListItem>
                <asp:ListItem Value="Premium" Text="Premium Muscles $59.99"></asp:ListItem>
                <asp:ListItem Value="PremiumPlus" Text="Premium Plus Muscles $69.99"></asp:ListItem>
            </asp:DropDownList>
            <br />
        <div id="Training_Type" runat="server">
            <h4>What are your favourite ways to train? (Select all that apply)</h4>
                <asp:CheckBox runat="server" ID="CardioTraining" Text="Cardio" />
                <asp:CheckBox runat="server" ID="StrengthTraining" Text="Strength" />
                <asp:CheckBox runat="server" ID="SizeTraining" Text="Very Big Muscles" />
            <br />
        </div>
            <h5>How did you hear about us? (Select all that apply)</h5>
                <asp:CheckBox runat="server" ID="social" Text="Social Media" />
                <asp:CheckBox runat="server" ID="web" Text="Website" />
                <asp:CheckBox runat="server" ID="friend" Text="Friend" />
                <asp:CheckBox runat="server" ID="customer" Text="An individual with very big muscles" />
                <asp:CheckBox runat="server" ID="other" Text="Other" />
                <asp:TextBox runat="server" ID="customAnswer" placeholder="Please share"></asp:TextBox>
            <br />
        
            <h5>Would you recommend us?</h5>
                <asp:RadioButton ID="YesButton" runat="server" Text="Yes" GroupName="answer"/>
                <asp:RadioButton ID="NoButton" runat="server" Text="No" GroupName="answer"/>
                <asp:TextBox runat="server" ID="No" placeholder="Why not?"></asp:TextBox>
            <br />
                <asp:Button ID="Button1" Text="Register" OnClick="SubmitBtn_Click" runat="server"/> 
        </div>
   
            <div ID="NewRegistration" runat="server">

            </div>
 

    </form>
</body>
</html>

